from fastapi import HTTPException
import logging
import os
from functools import lru_cache
from pydantic import BaseSettings


log = logging.getLogger("uvicorn")

        
class Prod_Settings(BaseSettings):
    
    # gerais
    CONFIG_DIR: str = os.path.dirname(os.path.abspath(__file__))
    SRC_DIR: str = os.path.dirname(CONFIG_DIR)
    ROOT_DIR: str = os.path.dirname(SRC_DIR)
    PA_DIGITAL_FOLDER_NAME = 'pa_digital_models'
    BIGQUERY_DATASET_NAME = 'fenix_raw'
    BIGQUERY_DATASET_TABLE = 'pa_digital_full'
    
    # Credenciais de acesso ao GCP.
    GCLOUD_PROJECT: str = 'cosmic-shift-235317'
    GCLOUD_BUCKET: str = 'prd-ml-models'
    GCLOUD_CREDENTIALS_FILEPATH: str = f'{CONFIG_DIR}/gcloud_credentials_prd.json'

class Qa_Settings(BaseSettings):
    
    # gerais
    CONFIG_DIR: str = os.path.dirname(os.path.abspath(__file__))
    SRC_DIR: str = os.path.dirname(CONFIG_DIR)
    ROOT_DIR: str = os.path.dirname(SRC_DIR)
    PA_DIGITAL_FOLDER_NAME = 'pa_digital_models'
    BIGQUERY_DATASET_NAME = 'fenix_raw'
    BIGQUERY_DATASET_TABLE = 'pa_digital_full'
    
    # Credenciais de acesso ao GCP.
    GCLOUD_PROJECT: str ='gesto-qa'
    GCLOUD_BUCKET: str ='qa-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH: str =f'{CONFIG_DIR}/machine-learning-qa-gesto-qa-dc1e6de3a282.json'

class Dev_Settings(BaseSettings):
    
    # gerais
    CONFIG_DIR: str = os.path.dirname(os.path.abspath(__file__))
    SRC_DIR: str = os.path.dirname(CONFIG_DIR)
    ROOT_DIR: str = os.path.dirname(SRC_DIR)
    PA_DIGITAL_FOLDER_NAME = 'pa_digital_models'
    BIGQUERY_DATASET_NAME = 'fenix_raw'
    BIGQUERY_DATASET_TABLE = 'pa_digital_full'
    
    # Credenciais de acesso ao GCP.
    GCLOUD_PROJECT: str ='laboratorio-eng-dados'
    GCLOUD_BUCKET: str ='dev-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH: str =f'{CONFIG_DIR}/gcloud_credentials_dev.json'


@lru_cache()
def get_settings(environment) -> BaseSettings:

    if environment == 'dev':
        log.info(f"Loading config settings from the environment: {environment}")
        return Dev_Settings()
    elif environment == 'qa':
        log.info(f"Loading config settings from the environment: {environment}")
        return Qa_Settings()
    elif environment == 'prod':
        log.info(f"Loading config settings from the environment: {environment}")
        return Prod_Settings()
    else:
        raise HTTPException(status_code=400, detail="Please insert dev, qa or prod in environment!")
