from fastapi import HTTPException
from src.data_prep.data_prep_utils import read_query, bq_query_to_df, select_nulls, format_df, cleaning_df, preprocessing_df, parse_pt_date
from src.prediction.predicting_functions import predicting_multinomialLR, post_processing_model
from src.utils.gcp_utils import upload_file, download_blob
from src.utils.api_utils import PandasToJson
import pandas as pd
import os

# run download model
def run_gcp_download_model(settings):
    
    source_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/lr_model.joblib'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/lr_model.joblib'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file .joblib downloaded from gcp')



# processo preprocessar textos
def preproc_pa_digital(df):
        
    # format df
    
    cols_format = ['idConsulta', 'dataConsulta', 'idBeneficiario_original', 
                   'idBeneficiario', 'id_pessoa', 'idEmpresa']
    
    formated_df = format_df(df, columns = cols_format)
    # clean df
    cleaned_df = cleaning_df(formated_df, column_to_clean = 'motivoConsulta')
    # preprocessing df
    processed_df = preprocessing_df(cleaned_df, text_column = 'motivoConsulta')
    
    return processed_df



# rodar os processos
def run(empresa_id, filter_date, settings):
    
     # data aquisition
    query_path = f'{settings.ROOT_DIR}/src/data_prep/queries/query_pa_digital_predict.txt'
    sql_query = read_query(query_path)
    sql_query = sql_query.replace('NUMBER_ID_EMPRESA', str(empresa_id))
    
    df = bq_query_to_df(query = sql_query, 
                        project = settings.GCLOUD_PROJECT, 
                        dataset = settings.BIGQUERY_DATASET_NAME, 
                        table = settings.BIGQUERY_DATASET_TABLE,
                        credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    if df.shape[0] == 0:
        raise HTTPException(status_code=400, detail="No data to predict. Please, enter a idEmpresa that exists in the database")
    
    # format date
    df['dataConsulta'] = df['dataConsulta'].apply(lambda x: parse_pt_date(x))
    
    # filtering date
    if filter_date != 'None':
        df = df[df['dataConsulta'] >= pd.to_datetime(filter_date)].reset_index(drop=True)
    
    # select nulls
    df_nulls = select_nulls(df, text_column = 'motivoConsulta')
    
    # preprocessing
    processed_df = preproc_pa_digital(df)
    
    ## predicting
    # download joblib
    run_gcp_download_model(settings)
    # prediction
    predicted_data = predicting_multinomialLR(processed_df, settings)
    
    # post processing
    cols_final = ['idConsulta', 'dataConsulta', 'idBeneficiario_original',
                  'idBeneficiario', 'id_pessoa', 'idEmpresa', 'classificacao']
    final_data = post_processing_model([predicted_data, df_nulls], cols_final)
    
    # to json
    json_path = f'{settings.ROOT_DIR}/src/ml_models/final_data.json'
    data = PandasToJson(final_data, json_path)
        
    # delete json
    os.remove(json_path)
    
    return data
