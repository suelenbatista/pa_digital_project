import pandas as pd
import os
from src.utils.gcp_utils import download_blob
from src.utils.api_utils import PandasToJson


# run download dictionary
def run_gcp_download_dictionary(settings):
    
    source_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/dict_pa.csv'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file dict_pa.csv downloaded from gcp')
    


# rodar o processo
def run(settings):
    
    # download dictionary
    run_gcp_download_dictionary(settings)
    
    # load dictionary
    dict_pa = pd.read_csv(f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv', encoding = 'utf-8').fillna('-')
    
    # to json
    json_path = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.json'
    data = PandasToJson(dict_pa, json_path)
    
    # delete json
    os.remove(json_path)
    
    return data
