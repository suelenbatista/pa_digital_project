from google.cloud import storage


# upload file to gcp
def upload_file(bucket_name, destination_blob_name, source_file_path, credentials):
    """Uploads a file to the bucket."""

    storage_client = storage.Client.from_service_account_json(json_credentials_path=credentials)
    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_path)

    print(f"File uploaded to {destination_blob_name}.")


# download file from gcp
def download_blob(bucket_name, source_blob_name, destination_file_name, credentials):
    """Downloads a file from the bucket."""
    
    storage_client = storage.Client.from_service_account_json(json_credentials_path=credentials)
    bucket = storage_client.bucket(bucket_name)
    
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)
    
    print(f"Blob {source_blob_name} downloaded to {destination_file_name}.")
