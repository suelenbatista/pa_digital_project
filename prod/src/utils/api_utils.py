from fastapi.responses import JSONResponse
import os
import json

# success response model
def ResponseModel(data, message):
    return JSONResponse(
        status_code=200, 
        content={"code": 200, "status": message, "data": data}
    )    


# errors response model
def ErrorResponseModel(code, message):
    return JSONResponse(
        status_code=code, 
        content={"code": code, "message": message}
    )


def PandasToJson(df, json_path):
    
    # write json
    with open(json_path, 'w', encoding='utf-8') as file:
        df.to_json(file, force_ascii=False)
    
    # read json
    file_json = open(json_path)
    data = json.load(file_json)
    
    return data