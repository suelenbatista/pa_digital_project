from google.cloud import bigquery
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
from unidecode import unidecode
import datetime


# read queries in .txt
def read_query(path_to_query):
    
    '''path_to_query: /path/to/query'''
    
    query_read = open(path_to_query, 'r', encoding='utf-8')
    sql_query = query_read.read()
    query_read.close()
    
    return sql_query


# query in bq
def bq_query_to_df(query, project, dataset, table, credentials):
    
    '''query: query on bq
       project: gcp project_id
       dataset: bq dataset_id
       table: bq table_id
       credentials: path/to/.json'''
    
    query = query.format(project=project, dataset=dataset, table=table)

    print(query)

    client = bigquery.Client.from_service_account_json(json_credentials_path=credentials)
    results_df = client.query(query).to_dataframe(progress_bar_type='tqdm')
    
    return results_df


# format indexed columns
def format_df(df, columns):
    
    '''Formata o dataset com os campos de interesse
    df: pandas dataframe
    columns: columns list to index'''
    
    df.set_index(columns, inplace=True)
    
    return df


# clean dataset nulls
def cleaning_df(df, column_to_clean):
    
    '''Delete nulls
       df: pandas dataframe
       column_to_clean: column necessary to clean'''
    
    dataframe = df[(df[column_to_clean].str.len()>=2) & \
                   (df[column_to_clean] != "null") & \
                   (df[column_to_clean] != " ")] \
                  .dropna(subset=[column_to_clean])
    
    return dataframe



# select nulls values
def select_nulls(df, text_column):
    
    '''Select lines with no text
    df: pandas dataframe
    text_column: column with texts'''
    
    nulos = df[df[text_column].isnull() | df[text_column].isin(['null', ''," "])]
    nulos.drop(columns=text_column) 
    
    return nulos



# preprocessing with nltk
def preprocessing_df(df, text_column):
    
    '''Pre processamento dos textos do dataframe
       df: pandas dataframe
       text_column: column with texts'''
    
    # nltk parameters
    nltk.download('punkt')
    nltk.download('stopwords')
    stop = stopwords.words('portuguese')
    
    
    # text pre preprocessing
    df[text_column] = df[text_column].apply(lambda x: re.sub('[0-9]|,|\.|/|$|\(|\)|-|\+|:|•', ' ', x)) \
                                     .apply(lambda x: unidecode(x)).str.lower() \
                                     .apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
    
    df['tokens'] = df[text_column].apply(nltk.word_tokenize) \
                                  .apply(', '.join)
    
    return df

# adjust dates
def parse_pt_date(date_string):
    '''Parses a date-time string and return datetime object
       The format is like this:
       'Seg, 21 Out 2013 22:14:36 -0200'
    '''
    
    MONTHS = {'jan': 1, 'fev': 2, 'mar': 3, 'abr': 4,  'mai': 5,  'jun': 6,
              'jul': 7, 'ago': 8, 'set': 9, 'out': 10, 'nov': 11, 'dez': 12}

    FULL_MONTHS = {'janeiro': 1,  'fevereiro': 2, u'março': 3,    'abril': 4,
                   'maio': 5,     'junho': 6,     'julho': 7,     'agosto': 8,
                   'setembro': 9, 'outubro': 10,  'novembro': 11, 'dezembro': 12}

    date_info = date_string.lower().split()
    
    if date_info.count('de') == 2 or len(date_info) == 3:
        if ',' in date_info[0]:
            date_string = date_string.split(',')[1]
        date_string = date_string.lower().replace('de ', '')
        date_info = date_string.lower().replace('.', '').split()
        
        day, month_pt, year = date_info
        if len(month_pt) == 3:
            month = MONTHS[month_pt]
        else:
            month = FULL_MONTHS[month_pt]
        date_iso = '{}-{:02d}-{:02d}'.format(year, int(month), int(day))
        date_object = datetime.datetime.strptime(date_iso, '%Y-%m-%d')
        return date_object
    else:
        _, day, month_pt, year, hour_minute_second, offset = date_info

        if offset.lower() == 'gmt':
            offset = '+0000'
        offset_signal = int(offset[0] + '1')
        offset_hours = int(offset[1:3])
        offset_minutes = int(offset[3:5])
        total_offset_seconds = offset_signal * (offset_hours * 3600 +
                                                offset_minutes * 60)
        offset_in_days = total_offset_seconds / (3600.0 * 24)

        month = MONTHS[month_pt]
        datetime_iso = '{}-{:02d}-{:02d}T{}'.format(year, month, int(day),
                hour_minute_second)
        datetime_object = datetime.datetime.strptime(datetime_iso,
                '%Y-%m-%dT%H:%M:%S')
        return datetime_object - datetime.timedelta(offset_in_days)