from fastapi import APIRouter
from endpoints import home_page, load_classes, pa_train, pa_predict, preview_dictionary, preview_crossvalidation, preview_confusionmatrix

router = APIRouter()

router.include_router(home_page.router, tags=["Home"])
router.include_router(load_classes.router, tags=["Load_Classes"])
router.include_router(preview_dictionary.router, tags=["PA_Dictionary"])
router.include_router(preview_crossvalidation.router, tags=["PA_CrossValidation_Results"])
router.include_router(preview_confusionmatrix.router, tags=["PA_ConfusionMatrix_Results"])
router.include_router(pa_train.router, tags=["PA_Train"])
router.include_router(pa_predict.router, tags=["PA_Predict"])

