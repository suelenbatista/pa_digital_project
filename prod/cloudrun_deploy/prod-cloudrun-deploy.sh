# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Set environment variables.
export FASTAPI_ENV=production

# Authenticate.
gcloud auth activate-service-account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--key-file src/config/gcloud_credentials_prod.json

# Submit build.
gcloud builds submit \
--tag gcr.io/cosmic-shift-235317/prod-pa-digital-service \
--account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--project cosmic-shift-235317

# Deploy and run container.
gcloud run deploy \
prod-pa-digital-service \
--image gcr.io/cosmic-shift-235317/prod-pa-digital-service \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--project cosmic-shift-235317 \
--memory 4Gi \
--timeout 14m