from fastapi import APIRouter, File, Form, UploadFile, HTTPException
from src import update_classes
from datetime import datetime
import os
from src.utils.api_utils import ResponseModel
from src.config.config import get_settings, Prod_Settings, Dev_Settings

router = APIRouter()

today = f"{datetime.now():%Y-%m-%d  %H:%M:%S}"

# carregar arquivo contendo as classes para treinamento
@router.post("/v1/load_classes", name="Atualizar classes", description="Atualizar as classes para o algoritmo classificar.")

async def load_classes(environment: str = Form(...), 
                       date: str = today, 
                       file: UploadFile = File(...)):
    
    '''Carregar arquivo contendo as classes para classificacao
    environment: env ou prod
    file: /path/to/file.csv'''
    
    settings = get_settings(environment)
    update_classes.run(settings, file) 
    
    data = {"env":environment, 
            "date":date, 
            "filename":file.filename,
            "file_content_type":file.content_type}
    
    return ResponseModel(data, "success")