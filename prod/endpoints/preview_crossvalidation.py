from fastapi import APIRouter, HTTPException
from starlette.responses import FileResponse
from src.config.config import get_settings, Prod_Settings, Dev_Settings
from src.utils.gcp_utils import download_blob
#from src.utils.api_utils import ResponseModel
#from src import get_dictionary

router = APIRouter()

#home page
@router.get("/v1/preview_crossvalidation/{environment}", 
            name = "Preview Cross Validation results", description = "PA Digital cross validation results.")
async def preview_crossvalidation(environment: str):
    """Get cross validation results from the model trained."""
    
    # load config
    settings = get_settings(environment)
    
    # download cross validation results
    source_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_results/cv_LRmodel_results.csv'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/cv_LRmodel_results.csv'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    # download file
    file_location = destination_file
    file_name = 'cross_validation_results.csv'
    
    return FileResponse(file_location, media_type='application/octet-stream',filename=file_name)