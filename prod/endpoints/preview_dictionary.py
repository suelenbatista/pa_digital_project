from fastapi import APIRouter, HTTPException
from src.config.config import get_settings, Prod_Settings, Dev_Settings
from src.utils.api_utils import ResponseModel
from src import get_dictionary

router = APIRouter()

#home page
@router.get("/v1/preview_dictionary/{environment}", 
            name = "Preview Dictionary", description = "PA Digital dictionary texts.")
async def preview_dictionary(environment: str):
    """Get dictionary used for model training."""
    
    # load config
    settings = get_settings(environment)
    
    # get dictionary
    data = get_dictionary.run(settings)
    
    return ResponseModel(data, "Success")
