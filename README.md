# PA Digital Project

## Developer Guide:
- dev folder: notebooks_dev (ml models and transformations), data (data used in the models), queries (google big query query .txt files)

- prod folder: api development

### How to use API:
#### Development:

In folder prod: `uvicorn main:app --reload`


- Curl's request:


- Upload dictionary: `curl -v POST -F 'environment=dev' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' http://127.0.0.1:8000/v1/load_classes`

Environments: dev, qa, prod.

- Get dictionary: `curl -v GET http://127.0.0.1:8000/v1/preview_dictionary/{environment}`


- Get cross validation results: `curl -v GET http://127.0.0.1:8000/v1/preview_crossvalidation/{environment}`


- Get confusion matrix results: `curl -v GET http://127.0.0.1:8000/v1/preview_confusionmatrix/{environment}`


- Train model: `curl -v POST -F 'environment=dev' http://127.0.0.1:8000/v1/pa_train`


- Predict model all data: `curl -v POST -F 'environment=dev' -F 'empresa_id=number_empresa_id' http://127.0.0.1:8000/v1/pa_predict`


- Predict model filtering period: `curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=YYYY-mm-dd' http://127.0.0.1:8000/v1/pa_predict`


#### Cloud run:

Home page: [https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app]


- Curl's request:


- Upload dictionary: `curl -v POST -F 'environment=dev' -F 'file=@/home/jupyter/pa_digital_project/dev/data/dict_pa.csv' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/load_classes`


- Get dictionary: `curl -v GET https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/preview_dictionary/dev`


- Get cross validation results: `curl -v GET https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/preview_crossvalidation/dev`


- Get confusion matrix results: `curl -v GET https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/preview_confusionmatrix/dev`


- Train model: `curl -v POST -F 'environment=dev' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/pa_train`


- Predict model all data: `curl -v POST -F 'environment=dev' -F 'empresa_id=number_empresa_id' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/pa_predict`


- Predict model filtering period: `curl -v POST -F 'environment=dev' -F 'empresa_id=159' -F 'filter_appointment_date=YYYY-mm-dd' https://dev-pa-digital-service-zm3msiaf7q-ue.a.run.app/v1/pa_predict`